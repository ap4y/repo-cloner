package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	yaml "gopkg.in/yaml.v2"
)

var (
	timeout     = flag.Int("timeout", 30, "number of seconds to wait in each clone step")
	cloneDepth  = flag.Int("depth", -1, "perform shallow clone with specified depth")
	destination = flag.String("clone_path", "", "location for cloned repositories")
	resultFile  = flag.String("out", "", "location of a report file. by default will write to stdout")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintln(flag.CommandLine.Output(), "Usage repo-cloner [flags] [repos.yml]:")
		flag.PrintDefaults()
	}
	flag.Parse()

	configPath := "repos.yml"
	if args := flag.Args(); len(args) != 0 {
		configPath = args[0]
	}

	repos, err := reposList(configPath)
	if err != nil {
		log.Fatal("Failed to parse yaml config: ", err)
	}

	checker, err := NewChecker(time.Duration(*timeout)*time.Second, *cloneDepth, *destination)
	if err != nil {
		log.Fatal("Failed to initialise: ", err)
	}
	report := checker.CheckRepositories(repos)

	if err := report.Write(*resultFile); err != nil {
		log.Fatal("Failed to write report file: ", err)
	}
}

func reposList(filepath string) ([]string, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, fmt.Errorf("failed to open file: %s", err)
	}
	defer file.Close()

	repos := []string{}
	if err := yaml.NewDecoder(file).Decode(&repos); err != nil {
		return nil, fmt.Errorf("failed to decode yml: %s", err)
	}

	return repos, nil
}
