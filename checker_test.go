package main

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var mockCloner = &MockRepoCloner{}

func TestCheckRepositoryTimeout(t *testing.T) {
	cloner := &MockRepoCloner{duration: 1}
	ch := &Checker{timeout: time.Millisecond, cloner: cloner}

	res := ch.CheckRepository("https://gitlab.com/ap4y/skycoin-node-sync-test.git")
	assert.Equal(t, "https://gitlab.com/ap4y/skycoin-node-sync-test.git", res.Repository)
	assert.False(t, res.Success)
	assert.NotNil(t, res.Duration)

	require.Len(t, cloner.args, 2)
}

func TestCheckRepositoryDestination(t *testing.T) {
	ch := &Checker{destination: os.TempDir(), timeout: time.Second, cloner: mockCloner}

	res := ch.CheckRepository("https://gitlab.com/ap4y/skycoin-node-sync-test.git")
	assert.Equal(t, "https://gitlab.com/ap4y/skycoin-node-sync-test.git", res.Repository)
	assert.True(t, res.Success)
	assert.NotNil(t, res.Duration)

	require.Len(t, mockCloner.args, 2)
	assert.Contains(t, mockCloner.args[1], os.TempDir())
}

func TestCheckRepositoryDepth(t *testing.T) {
	ch := &Checker{depth: 1, timeout: time.Second, cloner: mockCloner}

	res := ch.CheckRepository("https://gitlab.com/ap4y/skycoin-node-sync-test.git")
	assert.Equal(t, "https://gitlab.com/ap4y/skycoin-node-sync-test.git", res.Repository)
	assert.True(t, res.Success)
	assert.NotNil(t, res.Duration)

	require.Len(t, mockCloner.args, 4)
	assert.Equal(t, "--depth", mockCloner.args[2])
	assert.Equal(t, "1", mockCloner.args[3])
}

func TestCheckRepositoryMultiple(t *testing.T) {
	ch := &Checker{timeout: time.Second, cloner: mockCloner}

	report := ch.CheckRepositories(
		[]string{"https://gitlab.com/ap4y/skycoin-node-sync-test.git", "github.com/skycoin/skycoin"},
	)
	assert.Len(t, report.RepoResults, 2)
	assert.NotNil(t, report.Duration)
}

type MockRepoCloner struct {
	args     []string
	duration time.Duration
}

func (c *MockRepoCloner) Clone(args []string) error {
	c.args = args
	time.Sleep(c.duration * time.Second)
	return nil
}
