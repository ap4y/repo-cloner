package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Report struct {
	Duration    time.Duration `json:"duration"` // total duration across all checked repositories
	RepoResults []Result      `json:"results"`  // individual results for repositories
}

func (r *Report) Write(filepath string) error {
	var resultWriter io.Writer
	if filepath != "" {
		f, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0755)
		if err != nil {
			return fmt.Errorf("failed to create file: %s", err)
		}
		defer f.Close()

		resultWriter = f
	} else {
		resultWriter = os.Stdout
	}

	if err := json.NewEncoder(resultWriter).Encode(r); err != nil {
		return fmt.Errorf("failed to encode json result: %s", err)
	}

	return nil
}

type Result struct {
	Repository string        `json:"repository"` // url of the repository
	Success    bool          `json:"success"`    // indicates that clone was successful
	Duration   time.Duration `json:"duration"`   // clone duration
}

type RepoCloner interface {
	Clone(args []string) error
}

type CmdRepoCloner struct{}

func (c CmdRepoCloner) Clone(cloneArgs []string) error {
	args := append([]string{"clone"}, cloneArgs...)
	cmd := exec.Command("git", args...)
	if err := cmd.Run(); err != nil {
		out, err := cmd.CombinedOutput()
		if err != nil {
			return fmt.Errorf("failed to execute \"git %s\": %s.", strings.Join(args, " "), err)
		}

		return fmt.Errorf("failed to execute \"git %s\": %s. %s", strings.Join(args, " "), err, out)
	}

	return nil
}

type Checker struct {
	timeout     time.Duration
	depth       int
	destination string
	cloner      RepoCloner
}

func NewChecker(timeout time.Duration, depth int, destination string) (*Checker, error) {
	if destination != "" {
		return &Checker{timeout, depth, filepath.Clean(destination), &CmdRepoCloner{}}, nil
	}

	u, err := user.Current()
	if err != nil {
		return nil, fmt.Errorf("failed to resolve home directory: %s", err)
	}

	return &Checker{timeout, depth, u.HomeDir, &CmdRepoCloner{}}, nil
}

func (c *Checker) CheckRepositories(repos []string) *Report {
	startedAt := time.Now()
	report := &Report{RepoResults: []Result{}}

	var wg sync.WaitGroup
	results := make(chan Result, len(repos))
	for _, repo := range repos {
		wg.Add(1)
		go func(repo string) {
			defer wg.Done()
			results <- c.CheckRepository(repo)
		}(repo)
	}
	wg.Wait()
	close(results)

	for result := range results {
		report.RepoResults = append(report.RepoResults, result)
	}
	report.Duration = time.Now().Sub(startedAt)

	return report
}

func (c *Checker) CheckRepository(repo string) Result {
	log.Printf("Cloning %s...\n", repo)
	ctx, cancel := context.WithTimeout(context.Background(), c.timeout)
	defer cancel()

	dir, err := ioutil.TempDir(c.destination, "repo-cloner")
	if err != nil {
		log.Println("Failed to create temp dir: ", err)
		return Result{repo, false, 0}
	}
	defer os.RemoveAll(dir)

	success := false
	startedAt := time.Now()
	outCh := c.cloneRepository(ctx, repo, dir)
	select {
	case err := <-outCh:
		if err != nil {
			log.Printf("Failed to clone repo %s: %s\n", repo, err)
			break
		}

		success = true
	case <-ctx.Done():
		log.Printf("Failed to clone repo %s: %s\n", repo, ctx.Err())
		break
	}

	return Result{repo, success, time.Now().Sub(startedAt)}
}

func (c *Checker) cloneRepository(ctx context.Context, repo, dir string) <-chan error {
	errCh := make(chan error)

	go func() {
		args := []string{repo, dir}
		if c.depth > 0 {
			args = append(args, "--depth", strconv.Itoa(c.depth))
		}

		select {
		case <-ctx.Done():
		case errCh <- c.cloner.Clone(args):
			return
		}
	}()

	return errCh
}
